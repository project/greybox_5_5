<?php
/**
 * @file  Module 'greybox_5_5' implements the GreyBox 5.5 Overlaybrowser 
 * This modules is fairly similar to the Greybox Redux module, the overlay
 * GreyBox 5.5 is substantially different than the Redux version
 * 
 * @version 5.x-5.53
 * 
 * This module is produced under GPL licence. In short this means; 
 * You may copy, share, alter and use this file without any limitations.
 * 
 * GPL allows me to make the following request though:
 * If you make changes to this module, you do so because you need to; perhaps I need those as
 * well so inform me of the changes you made as to improve the module.
 * If you make additions to this module, you do so because the module doesn't do as much as
 * you need it to do; I might have overseen something. Please inform me of additions as well.
 *
 * You can mail me at:   http://drupal.org/user/114256/contact
 *
 * To-do list and Version history: please check end of file.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
 
/**
 * Implementation of hook_init().
 */
function greybox_5_5_init() {	
  greybox_5_5_add_files();
}

/**
 * Implementation of hook_perm().
 */
function greybox_5_5_perm() {
  return array('administer greybox');
} 

/**
 * Implementation of hook_menu().
 */
function greybox_5_5_menu($maycache) {
 $items = array();

  if ($maycache) {
    $items[] = array(
      'path' => 'admin/settings/greybox55',
      'title' => t('Greybox 5.5'),
      'description' => t('Greybox 5.5 settings'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'greybox_5_5_settings',
      'access' => user_access('administer greybox'),
      'type' => MENU_NORMAL_ITEM
    );
  }

  return $items;
}

/**
 * Implementation of settings form.
 */
function greybox_5_5_settings() {
  $form = array();

  $form["greybox_5_5_options"] = array(
    "#type" => "fieldset",
    "#title" => t("Greybox"),
    "#collapsible" => TRUE, 
    "#collapsed" => FALSE,
  );
  $form["greybox_5_5_options"]["greybox_5_5_window_width"]  = array(
    "#type" => "textfield",
    "#title" => t("Greybox popup window width(pixels)."),
    "#default_value" => variable_get('greybox_5_5_window_width', 470),
  );
  $form["greybox_5_5_options"]["greybox_5_5_window_height"]  = array(
    "#type" => "textfield",
    "#title" => t("Greybox popup window height(pixels)."),
    "#default_value" => variable_get('greybox_5_5_window_height', 600),
  );

  $form["greybox_5_5_examples"] = array(
    '#type' => 'markup',
    '#value' => '<div>'.greybox_5_5_examples().'</div>',
  );
  return system_settings_form($form);
}

/**
 * Adds files needed for GB55 to work. Also creates a GB_ROOT_DIR variable definition file
 * TO DO: Move this to the Drupal.settings. variable space.
 */
function greybox_5_5_add_files() {
  // Load required js and css files.
  $path = drupal_get_path('module', 'greybox_5_5');
  $width = variable_get('greybox_5_5_window_width', 470);
  $height = variable_get('greybox_5_5_window_height', 600);
  if (file_exists($path .'/greybox/gb_styles.css')) {
    drupal_add_css($path . '/greybox/gb_styles.css');
    if (!file_exists(file_directory_path() .'/gb_root_dir.js')) {
      greybox_5_5_dynamic_js_create(file_directory_path() .'/gb_root_dir.js');
    }
    drupal_add_js(file_directory_path() .'/gb_root_dir.js');
    drupal_add_js($path . "/greybox/AJS.js");
    drupal_add_js($path . "/greybox/AJS_fx.js");
    drupal_add_js($path . "/greybox/gb_scripts.js");
  }
}

/**
 * Example usage explained
 */
function greybox_5_5_examples() {
  $html = t('Use the following tags to enable greybox')
  .':<br />'
  .t('!fs for a full-screen popup (or functioncall greybox_5_5_fullscreen() in a module)', array('!fs' => 'rel="gb_page_fs[]"'))
  .'<br />'
  .t('!cs for a centered popup (width and height in pixels, or use functioncall greybox_5_5_center(width, height) in a module. Omit parameters to use above set values.)', array('!cs' => 'rel="gb_page_center[width, height]"'))
  .'<br />'
  .t('!ps for a picture show (use a settag to group photo\'s. Use functioncall greybox_5_5_imageset("settag") in a module. Omit parameters to use above set values.)', array('!ps' => 'rel="gb_imageset[settag]"'))
  .'<br />'
  .'<br />'
  .t('Remember, this module is the library only. You will need to use additional modules to combine this library with other functionality.')
  .'<br />'
  .t('To test greybox Fullscreen, click !here', array('!here' => l('here', 'http://google.com', greybox_5_5_fullscreen('attribute'))))
  .'<br />'
  .t('To test greybox Centered, click !here', array('!here' => l('here', 'http://google.com', greybox_5_5_center(null, null, 'attribute'))));
  return $html;
}

/**
 * Functions to use in your code; fullscreen HTML tag
 */
function greybox_5_5_fullscreen($mode = 'html') {
  switch ($mode) {
    case 'html':
      return ' rel="gb_page_fs[]" ';
    break;
    case 'attribute':
      return array('rel' => "gb_page_fs[]");
    break;
  }
}

/**
 * Functions to use in your code; center HTML tag with specific or generally configured window size
 */
function greybox_5_5_center($width = null, $height = null, $mode = 'html') {
  $width = $width ? $width : variable_get('greybox_5_5_window_width', 470);
  $height = $height ? $height : variable_get('greybox_5_5_window_height', 600);
  switch ($mode) {
    case 'html':
      return ' rel="gb_page_center['.$width.', '.$height.'] "';
    break;
    case 'attribute':
      return array('rel' => "gb_page_center[$width, $height]");
    break;
  }
}

/**
 * Functions to use in your code; image set
 */
function greybox_5_5_imageset($settag = 'demo', $mode = 'html') {
  switch ($mode) {
    case 'html':
      return ' rel="gb_imageset['.$settag.']" ';
    break;
    case 'attribute':
      return array('rel' => "gb_imageset[$settag]");
    break;
  }
}

/**
 * Callback to dynamicaly generate the setting javascript file
 * Obsolete since we're creating the file in the files folder
 * Kept for future referance
 */
function greybox_5_5_dynamic_js() {
  echo _greybox_5_5_dynamic_js();
  exit;
}

/** 
 * Function to write the setting javascript file
 */
function greybox_5_5_dynamic_js_create($file) {
  file_save_data(
    _greybox_5_5_dynamic_js(),
    $file,
    FILE_EXISTS_REPLACE
  );
}

/**
 * Build the GB_ROOT_DIR setting needed for Greybox 5.5
 */
function _greybox_5_5_dynamic_js() {
  $root_dir = "http";
  $root_dir .= $_SERVER[HTTPS] ? 's' : '';
  $root_dir .= '://';
  $root_dir .= $_SERVER[HTTP_HOST];
  $root_dir .= base_path();
  $root_dir .= drupal_get_path('module', 'greybox_5_5');
  $root_dir .= '/greybox/';
  
  return 'var GB_ROOT_DIR = "'.$root_dir.'";';
}

/**
 * Version History:
 * 5.x-5.5  :  Initial version with 5.5 version of Greybox isntalled
 * 5.x-5.53 :  Updated to 5.53 version of greybox
 *
 *
 * TODO:
 * Move GB_ROOT_DIR variable to Drupal.settings object.
 */